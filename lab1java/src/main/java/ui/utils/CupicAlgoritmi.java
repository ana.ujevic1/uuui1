package ui.utils;

import ui.dto.*;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.ToDoubleFunction;

// http://java.zemris.fer.hr/nastava/ui/apps/apps-20200313.pdf
public class CupicAlgoritmi {


        public static <S extends Comparable<S>> Optional<BasicNode<S>> breadthFirstSearch(S s0, Function<S, Set<S>> succ, Predicate<S> goal, PathTracker pathTracker) {

            Deque<BasicNode<S>> open = new LinkedList<>();
            Set<S> visited = new TreeSet<>();
            open.add(new BasicNode<>(s0, null));
            while(!open.isEmpty()) {
                BasicNode<S> n = open.removeFirst();
                if(goal.test(n.getState())) return Optional.of(n);
                visited.add(n.getState());
                pathTracker.setStatesVisited(pathTracker.getStatesVisited() + 1);
                for(S child : succ.apply(n.getState())) {
                    if(visited.contains(child)) continue;
                    open.addLast(new BasicNode<>(child, n));
                }
            }
            return Optional.empty();
        }


    public static <S> Optional<CostNode<S>> uniformCostSearch(S s0, Function<S, Set<StateCostPair<S>>> succ, Predicate<S> goal, PathTracker pathTracker) {
        Queue<CostNode<S>> open = new PriorityQueue<>();
        open.add(new CostNode<>(s0, null, 0.0));
        while(!open.isEmpty()) {
            CostNode<S> n = open.remove();
            pathTracker.setStatesVisited(pathTracker.getStatesVisited() + 1);
            if(goal.test(n.getState())) return Optional.of(n);
            for(StateCostPair<S> child : succ.apply(n.getState())) {
                open.add(new CostNode<>(child.getState(), n, n.getCost()+
                        child.getCost()));
            }
        }
        return Optional.empty();
    }

    public static <S> Optional<HeuristicNode<S>> aStarSearchWithVisited(S s0, Function<S, Set<StateCostPair<S>>> succ, Predicate<S> goal, ToDoubleFunction<S> heuristic, PathTracker pathTracker) {
        PriorityQueue<HeuristicNode<S>> open = new PriorityQueue<>(HeuristicNode.COMPARE_BY_TOTAL);
        open.add(new HeuristicNode<>(s0, null, 0.0, heuristic.applyAsDouble(s0)));
        Set<S> visited = new HashSet<>();
        while(!open.isEmpty()) {
            HeuristicNode<S> n = open.remove();
            if(goal.test(n.getState())) return Optional.of(n);
            visited.add(n.getState());
            pathTracker.setStatesVisited(pathTracker.getStatesVisited() + 1);
            for(StateCostPair<S> child : succ.apply(n.getState())) {
                if(visited.contains(child.getState())) continue;
                double cost = n.getCost()+child.getCost();
                double total = cost + heuristic.applyAsDouble(child.getState());
                boolean openHasCheaper = false;
                Iterator<HeuristicNode<S>> it = open.iterator();
                while(it.hasNext()) {
                    HeuristicNode<S> m = it.next();
                    if(!m.getState().equals(child.getState())) continue;
                    if(m.getTotalEstimatedCost() <= total) {
                        openHasCheaper = true;
                    } else {
                        it.remove();
                    }
                    break;
                }
                if(!openHasCheaper) {
                    HeuristicNode<S> childNode = new HeuristicNode<>(child.
                            getState(), n, cost, total);
                    open.add(childNode);
                }
            }
        }
        return Optional.empty();
    }

}
