package ui.utils;

import ui.dto.Graph;
import ui.dto.Heuristic;
import ui.dto.Neighbour;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class FileHandler {

    /**
     *
     * @param filepath ocekivani file gdje je definiran problem
     * @throws FileNotFoundException
     */
    public static Graph getGraphFromFile(String filepath) throws FileNotFoundException {

        File file = new File(filepath);
        Scanner scanner = new Scanner(file);

        List<String> lines = new ArrayList<>();
        while(scanner.hasNextLine()){
            String line = scanner.nextLine().strip();
            if(!line.startsWith("#")) lines.add(line);
        }

        String startState = lines.get(0);
        List<String> finalStates = List.of(lines.get(1).split(" "));

        Map<String, List<Neighbour>> neighbourMap = new HashMap<>();
        lines.remove(0);
        lines.remove(0);
        lines.forEach(l -> {
            // D: A,15 C,18 F,17 H,16 L,10 G,13 M,6
            String[] split = l.split(":");
            String label = split[0].trim();
            List<String> neighbourStringList = List.of();
            if(split.length >= 2){
                neighbourStringList = List.of(split[1].split(" "));
                neighbourStringList.forEach(String::trim);
            }

            List<Neighbour> neighbourList = new ArrayList<>();
            neighbourStringList.forEach(s -> {
                String[] neighbourSplit = s.split(",");
                if(neighbourSplit.length > 1){
                    String name = neighbourSplit[0];
                    int cost = Integer.parseInt(neighbourSplit[1]);
                    neighbourList.add(new Neighbour(name, cost));
                }
            });
            neighbourMap.put(label, neighbourList);
        });
        return new Graph(neighbourMap, finalStates, startState); //to do dodaj lista finalstate
    }

    /**
     *
     * @param filepath ocekivani file gdje je definirana heuristika
     * @throws FileNotFoundException
     */
    public static Heuristic getHeuristicFromFile(String filepath) throws FileNotFoundException {
        File file = new File(filepath);
        Scanner scanner = new Scanner(file);

        Map<String, Double> nameCostMap = new HashMap<>();
        while(scanner.hasNextLine()){

            String line;
            line = scanner.nextLine();
            line = line.strip();

            if(line.startsWith("#")) continue;

            //state: cost
            String[] split = line.split(": ");
            String label = split[0];
            String cost = split[1];
            nameCostMap.put(label, Double.valueOf(cost));

        }

        return new Heuristic(nameCostMap);
    }

}
