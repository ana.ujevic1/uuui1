package ui.utils;


import java.util.*;

/**
 * Prekorirano sa interneta !!!
 */

// https://oshanoshu.github.io/2021-02-23-Simple-Java-Command-Line-Argument-Parser-Implementation/
public class ArgumentParser {

    private List<String> args;
    private final HashMap<String, List<String>> map = new HashMap<>();
    private final Set<String> flags = new HashSet<>();

    public ArgumentParser(String[] arguments) {
        this.args = Arrays.asList(arguments);
        map();
    }

    // Return argument names
    public Set<String> getArgumentNames() {
        Set<String> argumentNames = new HashSet<>();
        argumentNames.addAll(flags);
        argumentNames.addAll(map.keySet());
        return argumentNames;
    }

    // Check if flag is given
    public boolean getFlag(String flagName) {
        return flags.contains(flagName);
    }

    // Return argument value for particular argument name
    public String[] getArgumentValue(String argumentName) {
        if(map.containsKey(argumentName))
            return map.get(argumentName).toArray(new String[0]);
        else
            return null;
    }

    // Map the flags and argument names with the values
    public void map() {
        for(String arg: args) {
            if(arg.startsWith("-")) {
                if (args.indexOf(arg) == (args.size() - 1)) {
                    flags.add(arg.replace("-", ""));
                }
                else if (args.get(args.indexOf(arg)+1).startsWith("-")) {
                    flags.add(arg.replace("-", ""));
                }
                else {
                    //List of values (can be multiple)
                    List<String> argumentValues = new ArrayList<>();
                    int i = 1;
                    while(args.indexOf(arg)+i != args.size() && !args.get(args.indexOf(arg)+i).startsWith("-"))
                    {
                        argumentValues.add(args.get(args.indexOf(arg)+i));
                        i++;
                    }
                    map.put(arg.replace("-", ""), argumentValues);
                }
            }
        }
    }

    // Getteri i setteri


    public List<String> getArgs() {
        return args;
    }

    public void setArgs(List<String> args) {
        this.args = args;
    }

    public HashMap<String, List<String>> getMap() {
        return map;
    }

    public Set<String> getFlags() {
        return flags;
    }

    @Override
    public String toString() {
        return "args: " + args + "\n" +
                "map: " + map + "\n" +
                "flags: " + flags + "\n";
    }
}
