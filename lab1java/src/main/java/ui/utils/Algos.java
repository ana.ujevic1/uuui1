package ui.utils;

import ui.dto.*;
import ui.dto.BasicNode;
import ui.dto.PathTracker;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;

import static ui.utils.CupicAlgoritmi.*;

public class Algos {


    private static LinkedList<String> nodePath(BasicNode<String> node) {
        LinkedList<String> path = new LinkedList<>();
        nodePathRecursive(path, node);
        return path;
    }

    private static void nodePathRecursive(LinkedList<String> path, BasicNode<String> node) {
        if(node.getParent()!=null) {
            nodePathRecursive(path, node.getParent());
        }
        path.addLast(node.getState());
    }

    public static PathTracker bfs(Graph graph, boolean print){

        // Creating predicate
        Predicate<String> isGoal = state-> (graph.getFinalStates().contains(state));
        String s0 = graph.getStartState();
        Function<String, Set<String>> succ = a -> graph
                .getNeighbourMap().get(a).stream().map(Neighbour::getLabel).collect(Collectors.toSet());


        PathTracker pathTracker = new PathTracker(graph, "BFS");
        Optional<BasicNode<String>> endNode = breadthFirstSearch(s0, succ, isGoal, pathTracker);

        if(endNode.isEmpty()){
            System.out.println("[FOUND_SOLUTION]: no");
            return pathTracker;
        }

        LinkedList<String> path = nodePath(endNode.get());
        pathTracker.setPath(path);
        if(print) pathTracker.printPath();
        return pathTracker;
    }
    public static PathTracker ucs(Graph graph, boolean print){

        Predicate<String> isGoal = state-> (graph.getFinalStates().contains(state));
        String s0 = graph.getStartState();
        Function<String, Set<StateCostPair<String>>> succ = a -> {
            try {
                return graph.getNeighbourMap().get(a).stream().map(n -> new StateCostPair<>(n.getLabel(), n.getCost())).collect(Collectors.toSet());
            } catch (NullPointerException e){
                return new HashSet<>();
            }
        };

        PathTracker pathTracker = new PathTracker(graph, "UCS");
        Optional<CostNode<String>> endNode = uniformCostSearch(s0, succ, isGoal, pathTracker);

        if(endNode.isEmpty()){
            if(print) pathTracker.printPath();
            return pathTracker;
        }

        LinkedList<String> path = nodePath(endNode.get());
        pathTracker.setPath(path);
        if(print) pathTracker.printPath();
        return pathTracker;
    }
    public static PathTracker astar(Graph graph, Heuristic h, boolean print){

        String s0 = graph.getStartState();
        Function<String, Set<StateCostPair<String>>> succ = a -> graph
                .getNeighbourMap().get(a).stream().map(n -> new StateCostPair<>(n.getLabel(), n.getCost())).collect(Collectors.toSet());
        Predicate<String> goal = state-> (graph.getFinalStates().contains(state));
        ToDoubleFunction<String> heuristic = key -> h.getFunction().get(key);

        PathTracker pathTracker = new PathTracker(graph, "ASTAR");
        Optional<HeuristicNode<String>> endNode = aStarSearchWithVisited(s0,succ,goal,heuristic, pathTracker);

        if(endNode.isEmpty()){
            if(print) pathTracker.printPath();
            return pathTracker;
        }

        LinkedList<String> path = nodePath(endNode.get());
        pathTracker.setPath(path);
        if(print) pathTracker.printPath();
        return pathTracker;
    }

    public static void checkOptimistic(Graph graph, Heuristic h){
//        //∀s ∈ S. h(s) ≤ h*(s)
        boolean conclusion = true;
        for (String s1 : graph.getNeighbourMap().keySet()){


            Predicate<String> isGoal = state-> (graph.getFinalStates().contains(state));
            Function<String, Set<StateCostPair<String>>> succ = a -> {
                try {
                    return graph.getNeighbourMap().get(a).stream().map(n -> new StateCostPair<>(n.getLabel(), n.getCost())).collect(Collectors.toSet());
                } catch (NullPointerException e){
                    return new HashSet<>();
                }
            };
            PathTracker pathTracker = new PathTracker(graph, "UCS");
            Optional<CostNode<String>> endNode = uniformCostSearch(s1, succ, isGoal, pathTracker);
            LinkedList<String> path = nodePath(endNode.get());
            pathTracker.setPath(path);
            pathTracker.calculate();


            Double hstar = pathTracker.getTotalCost();
            Double heuristic = h.getFunction().get(s1);
            if(heuristic <= hstar) {
                System.out.println("[CONDITION]: [OK] h("+s1+") <= h*: "+heuristic+" <= "+hstar);
            } else {
                conclusion = false;
                System.out.println("[CONDITION]: [ERR] h("+s1+") <= h*: "+heuristic+" <= "+hstar);
            }
        }

        if (conclusion) {
            System.out.println("[CONCLUSION]: Heuristic is optimistic.");
        } else {
            System.out.println("[CONCLUSION]: Heuristic is not optimistic.");
        }
    }

    public static void checkConsistent(Graph graph, Heuristic h){
        boolean conclusion = true;
        for (String s1 : graph.getNeighbourMap().keySet()){
            List<Neighbour> neighbourList = graph.getNeighbourMap().get(s1);
            for (Neighbour neighbour : neighbourList){
                String s2 = neighbour.getLabel();
                double c = neighbour.getCost();
                if(h.getFunction().get(s1) <= h.getFunction().get(s2) + c) {
                    System.out.println("[CONDITION]: [OK] h("+s1+") <= h("+s2+") + c: "+h.getFunction().get(s1)+" <= "+h.getFunction().get(s2)+" + "+c+"");
                } else {
                    conclusion = false;
                    System.out.println("[CONDITION]: [ERR] h("+s1+") <= h("+s2+") + c: "+h.getFunction().get(s1)+" <= "+h.getFunction().get(s2)+" + "+c+"");
                }
            }
        }

        if (conclusion) {
            System.out.println("[CONCLUSION]: Heuristic is consistent.");
        } else {
            System.out.println("[CONCLUSION]: Heuristic is not consistent.");
        }
    }

}

/**
 *  (npr., “# A-STAR istra_heuristic.txt”)
 * Nakon linije s kraticom algoritma, ako je algoritam pronasao rjesenje, za svaki od
 * algoritama potrebno je ispisati informacije o sljede cih pet elemenata:
 * 1. informaciju o tome je li rjesenje pronadeno (yes ili no), uz prefiks "[FOUND_SOLUTION]:",
 * 2. broj posje cenih stanja (velicinu skupa closed ), uz prefiks "[STATES_VISITED]:",
 * 3. duljinu pronadenog rjesenja, uz prefiks "[PATH_LENGTH]:",
 * 4. cijenu pronadenog rjesenja (u zapisu s jednom decimalom), uz prefiks "[TOTAL_COST]:",
 * 5. putanju pronadenog rjesenja (listu stanja od pocetnog do ciljnog odvojene nizom
 * znakova " => "), uz prefiks "[PATH]:".
 *
 * Ako rjesenje nije pronadeno, dovoljno je ispisati prvi element ("[FOUND_SOLUTION]:")
 * iz prethodne liste.
 */