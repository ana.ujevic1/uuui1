package ui.dto;

import java.util.HashMap;
import java.util.Map;


/**
 * Ova je klasa za heuristiku, nameCostMap radi preslikavanje stanje -> heuristika
 */
public class Heuristic {

    private final Map<String, Double> nameCostMap;


    public Heuristic(Map<String, Double> nameCostMap) {
        this.nameCostMap = nameCostMap;
    }

    // Getteri i setteri
    public Map<String, Double> getFunction() {
        return nameCostMap;
    }
}
