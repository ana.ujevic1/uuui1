package ui.dto;

import java.util.Comparator;

public class HeuristicNode<S> extends CostNode<S> {

    private double totalEstimatedCost;

    public static final Comparator<HeuristicNode<?>> COMPARE_BY_COST = Comparator.comparingDouble(CostNode::getCost);
    public static final Comparator<HeuristicNode<?>> COMPARE_BY_TOTAL = Comparator.comparingDouble(HeuristicNode::getTotalEstimatedCost);
    public static final Comparator<HeuristicNode<?>> COMPARE_BY_HEURISTICS = Comparator.comparingDouble(n -> n.getTotalEstimatedCost() - n.getCost());

    public HeuristicNode(S state, HeuristicNode<S> parent, double cost
            , double totalEstimatedCost) {
        super(state, parent, cost);
        this.totalEstimatedCost = totalEstimatedCost;
    }
    public double getTotalEstimatedCost() {
        return totalEstimatedCost;
    }
    @Override
    public HeuristicNode<S> getParent() {
        return (HeuristicNode<S>)super.getParent();
    }
    @Override
    public String toString() {
        return String.format("(%s,%.1f,%.1f)", state, cost,
                totalEstimatedCost);
    }
}
