package ui.dto;

//http://java.zemris.fer.hr/nastava/ui/apps/apps-20200313.pdf
public class CostNode<S> extends BasicNode<S> implements Comparable<CostNode<S>> {
    protected double cost;
    public CostNode(S state, CostNode<S> parent, double cost) {
        super(state, parent);
        this.cost = cost;
    }
    public double getCost() {
        return cost;
    }
    @Override
    public CostNode<S> getParent() {
        return (CostNode<S>)super.getParent();
    }

    @Override
    public String toString() {
        return "(" + state +" "+ cost +")";
    }

    @Override
    public int compareTo(CostNode<S> o) {
        return Double.compare(this.cost, o.cost);
    }
}
