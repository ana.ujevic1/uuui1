package ui.dto;

/**
 * Ova klasa sluzi da mozemo mapirati stanje na susjede zapakira nam ime i cost.
 * label: ime stanja/susjeda u grafu ; cost: cijenja prelaska
 */
public class Neighbour {
    private String label;
    private double cost;

    public Neighbour(String label, int cost) {
        this.label = label;
        this.cost = cost;
    }


    // Getteri i setteri
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }
}
