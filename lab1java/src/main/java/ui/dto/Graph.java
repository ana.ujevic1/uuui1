package ui.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Ova je klasa za formalni problem
 */
public class Graph {

    private final Map<String, List<Neighbour>> neighbourMap;
    private List<String> finalStates;
    private final String startState;

    public Graph(Map<String, List<Neighbour>> neighbourMap, List<String> finalStates, String startState) {
        this.neighbourMap = neighbourMap;
        this.finalStates = finalStates;
        this.startState = startState;
    }

    // Getteri i setteri


    public List<String> getFinalStates() {
        return finalStates;
    }

    public String getStartState() {
        return startState;
    }

    public Map<String, List<Neighbour>> getNeighbourMap() {
        return neighbourMap;
    }

    public void setFinalStates(List<String> finalStates) {
        this.finalStates = finalStates;
    }
}
