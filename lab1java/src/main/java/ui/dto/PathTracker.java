package ui.dto;

import java.util.LinkedList;
import java.util.Stack;
import java.util.StringJoiner;


/**
 * Klasa koja sluzi za pracenje puta
 */
public class PathTracker {

    private Graph graph;
    private LinkedList<String> path;
    private boolean foundSolution;
    private String algoName;
    private int statesVisited;
    private double totalCost;
    private BasicNode<String> endNode;

    public PathTracker(Graph graph, String algoName) {
        this.algoName = algoName;
        this.graph = graph;
        this.path = new LinkedList<>();
        this.foundSolution = false;
        this.statesVisited = 0;
        this.totalCost = 0;
    }

    public void calculate(){
        for (int i = 0; i < path.size()-1; i++) {
            String currState = path.get(i);
            String nextState = path.get(i+1);

            graph.getNeighbourMap().get(currState).forEach(n -> {
                if (n.getLabel().equals(nextState)){ this.totalCost += n.getCost(); }
            });
        }
    }

    public void printPath() {
        calculate();
        System.out.println("# " + algoName);
        System.out.println("[FOUND_SOLUTION]: " + (foundSolution ? "no" : "yes"));
        System.out.println("[STATES_VISITED]: " + statesVisited);
        System.out.println("[PATH_LENGTH]: " + path.size());
        System.out.println("[TOTAL_COST]: " + totalCost);

        StringJoiner joiner = new StringJoiner(" => ");
        path.forEach(joiner::add);
        System.out.println("[PATH]: " + joiner);

    }

    public Graph getGraph() {
        return graph;
    }

    public void setGraph(Graph graph) {
        this.graph = graph;
    }

    public void setPath(LinkedList<String> path) {
        this.path = path;
    }

    public String getAlgoName() {
        return algoName;
    }

    public void setAlgoName(String algoName) {
        this.algoName = algoName;
    }

    public double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }

    public boolean isFoundSolution() {
        return foundSolution;
    }

    public void setFoundSolution(boolean foundSolution) {
        this.foundSolution = foundSolution;
    }

    public int getStatesVisited() {
        return statesVisited;
    }

    public void setStatesVisited(int statesVisited) {
        this.statesVisited = statesVisited;
    }

    public LinkedList<String> getPath() {
        return path;
    }
}
