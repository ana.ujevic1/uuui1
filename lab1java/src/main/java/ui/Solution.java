package ui;

import ui.utils.Algos;
import ui.utils.ArgumentParser;
import ui.utils.FileHandler;
import ui.dto.*;


import java.io.FileNotFoundException;

public class Solution {
    public static void main(String[] args) throws FileNotFoundException {
        ArgumentParser parser = new ArgumentParser(args);


        if(parser.getMap().containsKey("alg") // algoritmi
            && parser.getMap().containsKey("ss")){
            switch (parser.getMap().get("alg").get(0)){
                case "bfs":
                    Graph graph = FileHandler.getGraphFromFile(parser.getMap().get("ss").get(0));
                    Algos.bfs(graph, true);
                    break;
                case "ucs":
                    graph = FileHandler.getGraphFromFile(parser.getMap().get("ss").get(0));
                    Algos.ucs(graph, true);
                    break;
                case "astar":
                    // heuristic check
                    if(!parser.getMap().containsKey("h")){
                        throw new RuntimeException("Nije prosljedjena heuristika za astar algoritam.");
                    }
                    graph = FileHandler.getGraphFromFile(parser.getMap().get("ss").get(0));
                    Heuristic heuristic = FileHandler.getHeuristicFromFile(parser.getMap().get("h").get(0));
                    Algos.astar(graph, heuristic, true);
                    break;
                default:
                    throw new RuntimeException("Nije prepoznat algoritam za zadane argumente.");
            }
        } else if(parser.getMap().containsKey("ss") // Provjera heuristike
                && parser.getMap().containsKey("h")
                && parser.getFlags().size() > 0){
            Graph graph = FileHandler.getGraphFromFile(parser.getMap().get("ss").get(0));
            Heuristic heuristic = FileHandler.getHeuristicFromFile(parser.getMap().get("h").get(0));
            if(parser.getFlag("checkconsistent")) Algos.checkConsistent(graph, heuristic);
            if(parser.getFlag("checkoptimistic"))  Algos.checkOptimistic(graph, heuristic);
        } else {
            throw new RuntimeException("Parsiranje argumenata je poslo po zlu." + parser);
        }
    }
}
